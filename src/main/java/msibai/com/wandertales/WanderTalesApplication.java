package msibai.com.wandertales;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WanderTalesApplication {

	public static void main(String[] args) {
		SpringApplication.run(WanderTalesApplication.class, args);
	}

}
