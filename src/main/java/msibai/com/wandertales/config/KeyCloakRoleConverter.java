package msibai.com.wandertales.config;

import java.util.*;
import java.util.stream.Collectors;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

public class KeyCloakRoleConverter implements Converter<Jwt, Collection<GrantedAuthority>> {

  @Override
  public Collection<GrantedAuthority> convert(Jwt jwt) {

    @SuppressWarnings("unchecked")
    Map<String, Object> realmAccess =
        Optional.ofNullable(jwt.getClaims().get("realm_access"))
            .map(map -> (Map<String, Object>) map)
            .orElse(Collections.emptyMap());

    @SuppressWarnings("unchecked")
    List<String> roles =
        Optional.ofNullable((List<String>) realmAccess.get("roles"))
            .orElse(Collections.emptyList());

    return roles.stream()
        .filter(Objects::nonNull) // Ensure non-null role names
        .map(roleName -> "ROLE_" + roleName)
        .map(SimpleGrantedAuthority::new)
        .collect(Collectors.toList());
  }
}
