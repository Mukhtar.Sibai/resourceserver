package msibai.com.wandertales.controllers;

import io.micrometer.common.util.StringUtils;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import msibai.com.wandertales.dtos.PostCreationDTO;
import msibai.com.wandertales.dtos.PostUpdateDTO;
import msibai.com.wandertales.entities.Post;
import msibai.com.wandertales.services.impls.PostServiceImpl;
import org.apache.tika.Tika;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/posts")
@RequiredArgsConstructor
public class PostController {

  private final PostServiceImpl postService;

  @GetMapping
  public ResponseEntity<List<Post>> getAllPosts() {
    List<Post> posts = postService.getAllPosts();
    return ResponseEntity.ok(posts);
  }

  @GetMapping("/{postID}")
  public ResponseEntity<Post> getPostByID(@PathVariable UUID postID) {
    Optional<Post> post = postService.getPostById(postID);
    return post.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
  }

  @PostMapping("/create")
  public ResponseEntity<String> createPost(
      @RequestPart("postCreationDTO") PostCreationDTO postCreationDTO,
      @RequestPart("imageFile") MultipartFile imageFile,
      Authentication authentication) {

    UUID userId = UUID.fromString(authentication.getName());

    if (StringUtils.isBlank(postCreationDTO.getTitle())) {
      return badRequestResponse("Title must be provided.");
    }

    if (StringUtils.isBlank(postCreationDTO.getContent())) {
      return badRequestResponse("Content must be provided.");
    }

    byte[] imageData;

    if (imageFile == null || imageFile.isEmpty()) {
      return badRequestResponse("Image must be provided.");
    }

    if (isImageFile(imageFile)) {

      try {
        imageData = imageFile.getBytes();
      } catch (IOException e) {
        return internalServerErrorResponse("Error processing image file.");
      }
    } else {
      return badRequestResponse("Uploaded file is not an image.");
    }

    // Sanitize the title and content fields
    String sanitizedTitle = sanitize(postCreationDTO.getTitle());
    String sanitizedContent = sanitize(postCreationDTO.getContent());

    // Create a new PostCreationDTO with sanitized fields
    PostCreationDTO sanitizedPostCreationDTO =
        new PostCreationDTO(sanitizedTitle, sanitizedContent, imageData);

    Post createdPost = postService.createPost(sanitizedPostCreationDTO, userId);

    return ResponseEntity.status(HttpStatus.CREATED)
        .body("Post created successfully. Post ID: " + createdPost.getId());
  }

  private ResponseEntity<String> badRequestResponse(String message) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
  }

  private ResponseEntity<String> internalServerErrorResponse(String message) {
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(message);
  }

  @PatchMapping("/{postId}/update")
  public ResponseEntity<String> updatePost(
      @PathVariable UUID postId,
      @RequestPart(value = "imageFile", required = false) MultipartFile imageFile,
      @RequestPart(value = "postUpdateDTO", required = false) PostUpdateDTO postUpdateDTO,
      Authentication authentication) {

    UUID userId = UUID.fromString(authentication.getName());

    PostUpdateDTO sanitizedPostUpdateDTO = new PostUpdateDTO();

    if (postUpdateDTO != null) {

      if (postUpdateDTO.getTitle() != null) {
        String sanitizedTitle = sanitize(postUpdateDTO.getTitle());
        sanitizedPostUpdateDTO.setTitle(sanitizedTitle);
      }

      if (postUpdateDTO.getContent() != null) {
        String sanitizedContent = sanitize(postUpdateDTO.getContent());
        sanitizedPostUpdateDTO.setContent(sanitizedContent);
      }
    }

    if (imageFile != null && !imageFile.isEmpty()) {

      // Validate and process the image update
      if (isImageFile(imageFile)) {

        try {
          byte[] imageData = imageFile.getBytes();
          sanitizedPostUpdateDTO.setImageData(imageData);
        } catch (IOException e) {
          return internalServerErrorResponse("Error processing image file!");
        }

      } else {
        return badRequestResponse("Uploaded file is not an image.");
      }
    }

    postService.updatePost(postId, sanitizedPostUpdateDTO, userId);

    return ResponseEntity.ok("Post updated successfully.");
  }

  @DeleteMapping("/{postId}/delete")
  public ResponseEntity<String> deletePost(
      @PathVariable UUID postId, Authentication authentication) {
    // Get the authenticated user's ID from the authentication object
    UUID userId = UUID.fromString(authentication.getName());

    boolean deleted = postService.deletePost(postId, userId);
    return deleted
        ? ResponseEntity.ok("Post deleted successfully.")
        : ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body("Post not found or user does not have permission to delete.");
  }

  // Sanitization method
  private String sanitize(String input) {
    PolicyFactory policy = Sanitizers.FORMATTING.and(Sanitizers.LINKS);
    return policy.sanitize(input);
  }

  // Check if the provided file is an image
  private boolean isImageFile(MultipartFile file) {
    try {
      return file != null
          && file.getContentType() != null
          && file.getContentType().startsWith("image")
          && new Tika().detect(file.getBytes()).startsWith("image");
    } catch (IOException e) {
      return false;
    }
  }
}
