package msibai.com.wandertales.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostCreationDTO {

  @NotNull(message = "Title cannot be null")
  @NotBlank(message = "Title cannot be blank")
  @Size(max = 255, message = "Title cannot exceed 255 characters")
  private String title;

  @NotNull(message = "Content cannot be null")
  @NotBlank(message = "Content cannot be blank")
  private String content;

  @NotNull(message = "Image data cannot be null")
  private byte[] imageData;
}
