package msibai.com.wandertales.dtos;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostUpdateDTO {

  @Size(max = 255, message = "Title cannot exceed 255 characters")
  private String title;

  private String content;

  private byte[] imageData;
}
