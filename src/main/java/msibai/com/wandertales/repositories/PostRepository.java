package msibai.com.wandertales.repositories;

import java.util.UUID;
import msibai.com.wandertales.entities.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, UUID> {}
