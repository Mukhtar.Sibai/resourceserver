package msibai.com.wandertales.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import msibai.com.wandertales.dtos.PostCreationDTO;
import msibai.com.wandertales.dtos.PostUpdateDTO;
import msibai.com.wandertales.entities.Post;

public interface PostService {
  List<Post> getAllPosts();

  Optional<Post> getPostById(UUID postId);

  Post createPost(PostCreationDTO postCreationDTO, UUID authorID);

  Post updatePost(UUID postId, PostUpdateDTO postUpdateDTO, UUID authorID);

  boolean deletePost(UUID postId, UUID authorID);
}
