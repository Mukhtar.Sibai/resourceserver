package msibai.com.wandertales.services.impls;

import jakarta.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.*;
import lombok.AllArgsConstructor;
import msibai.com.wandertales.dtos.PostCreationDTO;
import msibai.com.wandertales.dtos.PostUpdateDTO;
import msibai.com.wandertales.entities.Post;
import msibai.com.wandertales.repositories.PostRepository;
import msibai.com.wandertales.services.PostService;
import org.apache.tika.Tika;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@AllArgsConstructor
public class PostServiceImpl implements PostService {

  private PostRepository postRepository;

  @Override
  public List<Post> getAllPosts() {
    return postRepository.findAll();
  }

  @Override
  public Optional<Post> getPostById(UUID postId) {
    return postRepository.findById(postId);
  }

  @Override
  @PreAuthorize("(hasRole('USER') and #authorID == authentication.name)")
  public Post createPost(PostCreationDTO postCreationDTO, UUID authorID) {

    validatePostCreation(postCreationDTO);

    if (isNotValidImage(postCreationDTO.getImageData())) {
      throw new IllegalArgumentException("Invalid image data.");
    }

    String sanitizedTitle = sanitize(postCreationDTO.getTitle());
    String sanitizedContent = sanitize(postCreationDTO.getContent());

    Post post =
        Post.builder()
            .title(sanitizedTitle)
            .content(sanitizedContent)
            .imageData(postCreationDTO.getImageData())
            .createdAt(LocalDateTime.now())
            .authorID(authorID)
            .build();

    return postRepository.save(post);
  }

  @Override
  @PreAuthorize("(hasRole('USER') and #authorID == authentication.name)")
  public Post updatePost(UUID postId, PostUpdateDTO postUpdateDTO, UUID authorID) {

    Optional<Post> optionalPost = postRepository.findById(postId);
    Post existingPost =
        optionalPost.orElseThrow(
            () -> new EntityNotFoundException("Post not found with id: " + postId));

    if (!isUserAuthorized(existingPost, authorID)) {
      throw new AccessDeniedException("You are not authorized to update this post.");
    }

    // Null check for updates
    if (postUpdateDTO == null) {
      return existingPost; // Nothing to update
    }

    // Check if any field is modified
    boolean isModified = false;

    if (postUpdateDTO.getTitle() != null
        && !postUpdateDTO.getTitle().equals(existingPost.getTitle())) {
      existingPost.setTitle(sanitize(postUpdateDTO.getTitle()));
      isModified = true;
    }

    if (postUpdateDTO.getContent() != null
        && !postUpdateDTO.getContent().equals(existingPost.getContent())) {
      existingPost.setContent(sanitize(postUpdateDTO.getContent()));
      isModified = true;
    }

    if (postUpdateDTO.getImageData() != null
        && !Arrays.equals(postUpdateDTO.getImageData(), existingPost.getImageData())) {
      // Validate and update image data
      if (isNotValidImage(postUpdateDTO.getImageData())) {
        throw new IllegalArgumentException("Invalid image data.");
      }

      existingPost.setImageData(postUpdateDTO.getImageData());
      isModified = true;
    }

    // Update modified timestamp only if any field is modified
    if (isModified) {
      existingPost.setModifiedAt(LocalDateTime.now());
    }

    return postRepository.save(existingPost);
  }

  @Override
  @PreAuthorize("hasRole('ADMIN') or (hasRole('USER') and #authorID == authentication.name)")
  public boolean deletePost(UUID postId, UUID authorID) {

    Optional<Post> optionalPost = postRepository.findById(postId);
    Post existingPost =
        optionalPost.orElseThrow(
            () -> new EntityNotFoundException("Post not found with id: " + postId));

    if (isUserAuthorized(existingPost, authorID) || isAdminUser()) {
      postRepository.delete(existingPost);
    } else {
      throw new AccessDeniedException("You are not authorized to update this post.");
    }

    return true;
  }

  private void validatePostCreation(PostCreationDTO postCreationDTO) {
    if (postCreationDTO == null
        || postCreationDTO.getImageData() == null
        || postCreationDTO.getImageData().length == 0) {
      throw new IllegalArgumentException(
          "Invalid postCreationDTO. Ensure title, content, and a non-empty file are provided.");
    }
  }

  private boolean isNotValidImage(byte[] imageData) {
    // Validate content type
    String contentType = new Tika().detect(imageData);
    return !StringUtils.hasText(contentType) || !contentType.startsWith("image");
  }

  private String sanitize(String input) {
    PolicyFactory policy = Sanitizers.FORMATTING.and(Sanitizers.LINKS);
    return policy.sanitize(input);
  }

  private boolean isUserAuthorized(Post existingPost, UUID authorID) {
    return Objects.equals(existingPost.getAuthorID(), authorID);
  }

  private boolean isAdminUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return authentication.getAuthorities().stream()
        .anyMatch(authority -> authority.getAuthority().equals("ROLE_ADMIN"));
  }
}
